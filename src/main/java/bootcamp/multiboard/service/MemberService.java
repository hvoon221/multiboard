package bootcamp.multiboard.service;

import bootcamp.multiboard.domain.Board;
import bootcamp.multiboard.domain.Member;
import bootcamp.multiboard.domain.vo.LoginDto;
import bootcamp.multiboard.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public Member login(LoginDto dto) {
        return memberRepository.findByUseridAndPw(dto.getUserid(), dto.getPw())
                .orElseThrow(() -> new RuntimeException("해당하는 사용자가 존재하지 않습니다."));
    }

}
