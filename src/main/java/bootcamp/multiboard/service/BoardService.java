package bootcamp.multiboard.service;

import bootcamp.multiboard.domain.Board;
import bootcamp.multiboard.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public Board registerBoard(Board board){
        return boardRepository.insert(board);
    }

    public List<Board> findAllBoard(){
        return boardRepository.findAll();
    }

    public void findBoard(String boardId){
        boardRepository.findById(boardId);
    }

    public void modifyBoard(Board board){
        boardRepository.insert(board);
    }

    public void removeBoard(String boardId){
        boardRepository.deleteById(boardId);
    }
}
