package bootcamp.multiboard.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter @Setter
@NoArgsConstructor
@Document(collection = "MEMBER")
public class Member extends Entity{

    private String userid;
    private String name;
    private String pw;

    public Member(String name, String userid, String pw){
        super();
        this.name = name;
        this.userid = userid;
        this.pw = pw;
    }

}
