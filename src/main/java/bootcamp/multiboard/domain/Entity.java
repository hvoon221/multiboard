package bootcamp.multiboard.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.UUID;

@Getter
@Setter
public abstract class Entity {
	//
	@Id
	protected String id;

	protected Entity() {
		//
		this.id = UUID.randomUUID().toString();
	}

}
