package bootcamp.multiboard.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "BOARD")
public class Board extends Entity {
    private String title;
    private String content;
    private String boardType;
    private String writer;

    public Board(String title, String content, String boardType, String writer){
        super();
        this.title = title;
        this.content = content;
        this.boardType = boardType;
        this.writer = writer;
    }

}
