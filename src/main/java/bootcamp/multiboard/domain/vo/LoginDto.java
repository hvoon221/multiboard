package bootcamp.multiboard.domain.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginDto {

    private String userid;
    private String pw;

    public LoginDto( String userid, String pw){
        super();
        this.userid = userid;
        this.pw = pw;
    }
}
