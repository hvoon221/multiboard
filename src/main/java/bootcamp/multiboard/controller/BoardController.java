package bootcamp.multiboard.controller;

import bootcamp.multiboard.domain.Board;
import bootcamp.multiboard.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor

@RequestMapping("/api/board")
public class BoardController {

    private final BoardService boardService;

    @GetMapping()
    public List<Board> findAllBoard(){
        return boardService.findAllBoard();
    }

    @PostMapping("/register")
    public Board registerBoard(@RequestBody Board board){
        return boardService.registerBoard(board);
    }

//    @PutMapping("/{boardId}")
//    public void modify(@PathVariable Board board) {
//        boardService.modifyBoard(board);
//    }
//
//    @DeleteMapping("/{boardId}")
//    public void delete(@PathVariable String boardId) {
//        boardService.removeBoard(boardId);
//    }
}
