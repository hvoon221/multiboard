package bootcamp.multiboard.controller;

import bootcamp.multiboard.domain.Member;
import bootcamp.multiboard.domain.vo.LoginDto;
import bootcamp.multiboard.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/member")
public class MemberController {

    private final MemberService memberService;

    @PostMapping("/login")
    public Member login(@RequestBody LoginDto dto) {
        return memberService.login(dto);
    }

}
