package bootcamp.multiboard.repository;

import bootcamp.multiboard.domain.Member;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends MongoRepository<Member, String>{
    //
    Optional<Member> findByUseridAndPw(String userid, String pw);
}
